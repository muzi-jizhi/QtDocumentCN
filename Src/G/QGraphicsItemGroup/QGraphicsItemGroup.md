
# QGraphicsItemGroup Class
The QGraphicsItemGroup class provides a container that treats a group of items as a single item. 

QGraphicsItemGroup类提供了一个容器，使得一组[GraphicsItem](../../P/GraphicsItem.md)可以被当做一个整体。

|   属性 | 方法                                                         |
| -----: | :----------------------------------------------------------- |
| 头文件 | `#include <QGraphicsItemGroup>`                               |
|  qmake | `QT+=widgets`                                                   |
| Since | Qt 4.2 |
| 继承自 | [GraphicsItem](../../P/GraphicsItem.md) |

## 公共成员类型

| 类型 | 方法                                                         |
| :--: | :----------------------------------------------------------- |
|enum | anonymous { Type } |

## 公共成员函数

| 返回类型 | 函数名                                                       |
| -------: | :----------------------------------------------------------- |
|	       |QGraphicsItemGroup([GraphicsItem](../../P/GraphicsItem.md) *parent = nullptr)|
|          |...|

---

## 详细介绍

The QGraphicsItemGroup class provides a container that treats a group of items as a single item.

QGraphicsItemGroup类提供了一个容器，使得一组[GraphicsItem](../../P/GraphicsItem.md)可以被当做一个整体。

A QGraphicsItemGroup is a special type of compound item that treats itself and all its children as one item (i.e., all events and geometries for all children are merged together). It's common to use item groups in presentation tools, when the user wants to group several smaller items into one big item in order to simplify moving and copying of items.

QGraphicsItemGroup是一个特殊的图形对象组合，它把自己和它的所有子对象视为一个对象。（例如，所有子对象的事件和图形结构都被合并到一起）

If all you want is to store items inside other items, you can use any QGraphicsItem directly by passing a suitable parent to setParentItem().

如果你仅仅是想把一些对象存储在另一些对象里，你可以直接调用setParentItem()函数为前者指定一个合适的父对象。

The boundingRect() function of QGraphicsItemGroup returns the bounding rectangle of all items in the item group. QGraphicsItemGroup ignores the ItemIgnoresTransformations flag on its children (i.e., with respect to the geometry of the group item, the children are treated as if they were transformable).

QGraphicsItemGroup的boundingRect()函数返回它包含的所有子对象的边界矩形。QGraphicsItemGroup无视其子对象的ItemIgnoresTransformations标志（例如，为了保持组合对象的图形结构，所有子对象都会被视为可以进行变换）。

There are two ways to construct an item group. The easiest and most common approach is to pass a list of items (e.g., all selected items) to QGraphicsScene::createItemGroup(), which returns a new QGraphicsItemGroup item. The other approach is to manually construct a QGraphicsItemGroup item, add it to the scene calling QGraphicsScene::addItem(), and then add items to the group manually, one at a time by calling addToGroup(). To dismantle ("ungroup") an item group, you can either call QGraphicsScene::destroyItemGroup(), or you can manually remove all items from the group by calling removeFromGroup().

有两种方法用以构造一个对象组。最容易且常用的办法是向QGraphicsScene::createItemGroup()函数传入一个对象列表（如所有被选中的对象），该函数返回一个QGraphicsItemGroup。另一种办法是手动构造一个QGraphicsItemGroup，调用QGraphicsScene::addItem()将其加入场景，然后通过调用addToGroup()将场景中的对象逐一添加到组内。要解散一个对象组，可以通过调用QGraphicsScene::destroyItemGroup()或调用removeFromGroup()手动从组内移除对象。

--- 

# 成员类型文档

...

