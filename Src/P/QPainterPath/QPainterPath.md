
# QPainterPath Class
The QPainterPath class provides a container for painting operations, enabling graphical shapes to be constructed and reused.

QPainterPath类提供了一个绘图操作容器，使绘图图形能够被构造和重用。

|   属性 | 方法                                                         |
| -----: | :----------------------------------------------------------- |
| 头文件 | `#include <QPainterPath>`                               |
|  qmake | `QT+=gui`                                                   |

## 公共成员类型

| 类型 | 方法                                                         |
| :--: | :----------------------------------------------------------- |
| class | Element |
|enum | ElementType { MoveToElement, LineToElement, CurveToElement, CurveToDataElement } |

## 公共成员函数

| 返回类型 | 函数名                                                       |
| -------: | :----------------------------------------------------------- |
|	       |QPainterPath()|
|          |...|

## 非成员函数

| 返回类型 | 函数名                                                       |
| -------: | :----------------------------------------------------------- |
|QDataStream &|operator<<(QDataStream &stream, const QPainterPath &path)|
|QDataStream &|operator>>(QDataStream &stream, QPainterPath &path)|

---

## 详细介绍

The QPainterPath class provides a container for painting operations, enabling graphical shapes to be constructed and reused.

QPainterPath类提供了一个绘图操作容器，使绘图图形能够被构造和重用。

A painter path is an object composed of a number of graphical building blocks, such as rectangles, ellipses, lines, and curves. Building blocks can be joined in closed subpaths, for example as a rectangle or an ellipse. A closed path has coinciding start and end points. Or they can exist independently as unclosed subpaths, such as lines and curves.

绘图路径是一个由若干矩形、椭圆、直线或曲线等绘图构建单元组成的对象。绘图构建单元可以被加入到闭合子路径中（如矩形或椭圆）。闭合路径的起点和终点重合，否则就是非闭合路径，如直线或曲线。

A QPainterPath object can be used for filling, outlining, and clipping. To generate fillable outlines for a given painter path, use the QPainterPathStroker class. The main advantage of painter paths over normal drawing operations is that complex shapes only need to be created once; then they can be drawn many times using only calls to the QPainter::drawPath() function.

PainterPath对象可以用来填充、标注轮廓或剪切。要使用绘图路径生成一个可填充的轮廓，可以使用[QPainterPathStroker](../../P/QPainterPathStroker.md)类。相较于通常的绘图操作，使用绘图路径最主要的优势是对于复杂形状只需要创建一次且之后可以直接调用[QPainter::drawPath()](../../P/QPainter.md)来实现多次绘制。

QPainterPath provides a collection of functions that can be used to obtain information about the path and its elements. In addition it is possible to reverse the order of the elements using the toReversed() function. There are also several functions to convert this painter path object into a polygon representation. 

QPainterPath类提供了一系列函数用于获取关于路径和其元素的信息。额外的，通过调用[toReversed()](../../P/QPainterPath.md)函数，可以颠倒元素顺序。还有一些函数可以将绘图路径转换为多边形形式。

--- 

# 成员类型文档

...

